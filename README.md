# Host a static website with container
## Update file index.html with your content
# Build the docker image
Docker build . -t tranhuuhoa/docker-html:v1
docker login
docker push tranhuuhoa/docker-html:v1
kubectl run my-service --image=tranhuuhoa/docker-html:v1 --port=80
# run a new container 
Docker run -d docker-html:v1

kubectl create deployment my-service --image=tranhuuhoa/docker-html:v1 --replicas=3
docker build . -t tranhuuhoa/docker-html:v2
docker push tranhuuhoa/docker-html:v2

kubectl set image deployment/my-service docker-html=tranhuuhoa/docker-html:v2